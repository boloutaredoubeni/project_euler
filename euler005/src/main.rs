static N: u64 = 20;

fn main() {
    println!("The answer is {}", euler005(N));
}

fn euler005(x: u64) -> u64 {
    let mut n = 1;
    let gcd = |mut a: u64, mut b: u64| {
        while b != 0 {
            let t = b;
            b = a % b;
            a = t;
        }
        a
    };
    let lcm = |a, b| { (a*b) / gcd(a, b)};
    for i in 2..x+1 {
        n = lcm(n, i);
    }
    n
}

#[cfg(test)]
mod test {
    #[test]
    fn test_case() {
        assert_eq!(2520, super::euler005(10));
    }
}
