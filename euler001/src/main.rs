/// # [Project Euler](https://projecteuler.net/)
/// [Multiples of 3 and 5](https://projecteuler.net/problem=2)
// Upper bound
static N: u32 = 1000;

fn main() {
    let y = euler001(N);
    println!("The answer is {}", y);
}

fn euler001(n: u32) -> u32 {
    // Make an iterator for numbers [1,n)
    (1..n)
    // Filter out if they are multiples of 3 and 5
    .filter(|&i| i % 3 == 0 || i % 5 == 0)
    .inspect(|&i| println!("{} is divisible by 3 or 5", i))
    // Then sum using accumulator pattern
    .fold(0, |sum, x| sum + x)
}

#[cfg(test)]
mod test {
    #[test]
    fn multiples_of_3_and_5() {
        // Testcase from project euler
        assert_eq!(23, euler001(10))
    }
}
