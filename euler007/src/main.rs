static N: usize = 10_001;

struct Primes {
    p: u64,
    primes: Vec<u64>,
}

impl Primes {
    fn new() -> Primes {
        Primes{ p: 2, primes: Vec::new() }
    }

    fn is_prime(&self, p: u64) -> bool {
        !self.primes.iter().any(|&i| p % i == 0)
    }
}

impl Iterator for Primes {
    type Item = u64;

    fn next(&mut self) -> Option<u64> {
        let p: u64 = if self.p == 2 {
            3
        } else {
            let mut n = self.p + 2;
            while !self.is_prime(n) {
                n += 2;
            }
            n
        };
        self.primes.push(p);
        self.p = p;
        Some(p)
    }
}

fn main() {
    let ans = euler007(N);
    println!("The answer is {}", ans);
}

fn euler007(n: usize) -> u64 {
    let mut p_gen = Primes::new();
    p_gen.nth(n-1).unwrap()
}

#[cfg(test)]
mod test {
    #[test]
    fn get_primes() {
        let mut prime_gen = super::Primes::new();
        assert_eq!(2, prime_gen.p);
        assert_eq!(3, prime_gen.next().unwrap());
        assert_eq!(5, prime_gen.next().unwrap());
        assert_eq!(7, prime_gen.next().unwrap());
        assert_eq!(11, prime_gen.next().unwrap());
        assert_eq!(13, prime_gen.next().unwrap());
    }
}
