extern crate num;

use num::pow;

static N: u64 = 100;

fn main() {
    println!("The answer is {}", euler006(N));
}

fn euler006(x: u64) -> u64 {
    let sum_of_squares = (1..x+1)
        .map(|i| num::pow(i, 2))
        .fold(0, |sum, i| sum + i);
    let square_of_sum = num::pow(
        (1..x+1).fold(0, |sum, i| sum + i),2
    );
    println!("{0} {1}", sum_of_squares, square_of_sum);
    square_of_sum - sum_of_squares
}

#[cfg(test)]
mod test {
    #[test]
    fn diff_to_ten() {
        assert_eq!(super::euler006(10), 2640);
    }
}
