// #![feature(step_by, core)]
static N: i32 = 999;

fn main() {
    let ans = euler004();
    // TODO: Uncoment when euler004 is Option<i32>
    // println!("The answer is {}", ans.unwrap());
    println!("The answer is {}", ans);
}

fn euler004() -> i32 {
    // TODO: use when core::iter::step_by is stable
    // TODO: change from F -> i32 to F -> Option<i32>
    let mut palindrome = 101;
    // for a in (999..99).step_by(-1) {
    //     for b in (a..99).step_by(-1) {
    //         let c = a * b;
    //         if c > palindrome && is_palindrome(c) {
    //             return Some(c);
    //         }
    //     }
    // }
    // None
    for a in (99..N) {
        for b in (a..N) {
            let c = a * b;
            if c > palindrome && is_palindrome(c) {
                palindrome = c;
            }
        }
    }
    return palindrome
}

fn is_palindrome(p: i32) -> bool {
    let mut s = p.to_string();
    unsafe {
        let vec = s.as_mut_vec();
        vec.reverse();
    }
    p.to_string() == s
}

#[cfg(test)]
mod test {

    #[test]
    fn test_name() {
        assert_eq!(true, super::is_palindrome(99));
        assert_eq!(true, super::is_palindrome(999));
        assert_eq!(true, super::is_palindrome(101));
        assert_eq!(true, super::is_palindrome(906609));
        assert_eq!(false, super::is_palindrome(789));
    }
}
