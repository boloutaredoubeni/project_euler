static N: u64 = 600_851_475_143;

fn main() {
    let n = N.clone();
    let y = euler003(n);
    println!("The answer is {}", y);
}

fn euler003(mut x: u64) -> u64 {
    // TODO: use iterator
    let mut i = 2u64;
    while i * i < x {
        while x % i == 0 {
            x /= i;
        }
        i += 1;
    }
    x
}

#[cfg(test)]
mod test {
    #[test]
    fn prime_factors() {

    }
}
