/// # [Project Euler](https://projecteuler.net/)
/// [Even Fibonacci numbers](https://projecteuler.net/problem=1)
static N: u32 = 4_000_000;

struct Fibonacci {
    a: u32,
    b: u32,
}

impl Iterator for Fibonacci {
    type Item = u32;
    fn next(&mut self) -> Option<u32> {
        let c = self.a + self.b;
        self.a = self.b;
        self.b = c;

        Some(self.a)
    }
}

fn fib() -> Fibonacci {
    Fibonacci {a: 1, b: 1}
}

fn main() {
    let ans = euler002();
    println!("The answer is {}", ans);
}

fn euler002() -> u32 {
    // create an iterator
    let f = fib();
    // Take While i < 4,000,000
    f.take_while(|&i| i < N)
    // .inspect(|&i| println!("took {}", i))
    // filter out if i mod 2 = 0
    .filter(|&i| i % 2 == 0)
    // .inspect(|&i| println!("filtered {}", i))
    // sum the values
    .fold(0, |sum, x| sum + x)
    // And that value is returned
}

#[cfg(test)]
mod test {
    #[test]
    fn get_10_fib() {
        let mut f = super::fib();
        assert_eq!(Some(1), f.next());
        assert_eq!(Some(2), f.next());
        assert_eq!(Some(3), f.next());
        assert_eq!(Some(5), f.next());
        assert_eq!(Some(8), f.next());
        assert_eq!(Some(13), f.next());
        assert_eq!(Some(21), f.next());
        assert_eq!(Some(34), f.next());
        assert_eq!(Some(55), f.next());
        assert_eq!(Some(89), f.next());
    }
}
